package fixture

import (
	_ "embed"
)

//go:embed semgrep.sarif
var semgrepSarif []byte

//go:embed security-scan.sarif
var securityCodeScanSarif []byte

//go:embed gl-sast-report.json
var sastJson []byte

//go:embed multi-run.sarif
var multiRunSarif []byte

//go:embed multi-run-actual.json
var multiRunCodeQuality string

type Fixtures struct {
	path string
}

func SemgrepSarif() []byte {
	return semgrepSarif
}

func SecurityCodeScan() []byte {
	return securityCodeScanSarif
}

func Sast() []byte {
	return sastJson
}

func MultiRunSarif() []byte {
	return multiRunSarif
}

func MultiRunCodeQuality() string {
	return multiRunCodeQuality
}
