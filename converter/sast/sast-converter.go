package sast

import (
	"github.com/owenrumney/go-sarif/v2/sarif"
	"sarif-converter/converter/sast/patch"
	"sarif-converter/meta"
	"sarif-converter/now"
	"sarif-converter/sarifreport/report"
	"sarif-converter/sast"
)

type SastConverter struct {
	time     now.TimeProvider
	metadata meta.Metadata
}

var Sast = "sast"

func (c SastConverter) Convert(input []byte) ([]byte, error) {
	raw, err := sarif.FromBytes(input)
	if err != nil {
		return []byte{}, err
	}
	patch.NewPatch(raw).Patch()
	sarifReport := report.NewReport(raw)

	r, err := sast.ConvertFrom(sarifReport, c.time, c.metadata)
	if err != nil {
		return nil, err
	}

	return r.Fix().Json()
}

func NewSastConverter(time now.TimeProvider, metadata meta.Metadata) SastConverter {
	return SastConverter{
		time:     time,
		metadata: metadata,
	}
}
