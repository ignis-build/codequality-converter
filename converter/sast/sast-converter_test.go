package sast

import (
	_ "embed"
	"github.com/stretchr/testify/assert"
	"sarif-converter/testing/fixture"
	"testing"
)

//go:embed sarif.json
var source []byte

//go:embed sast.json
var actual string

func TestConvertToSast(t *testing.T) {
	report, _ := NewSastConverterForTest().Convert(fixture.SemgrepSarif())

	assert.Equal(t, string(fixture.Sast()), string(report))
}

func TestConvert_SastMetadata(t *testing.T) {
	report, _ := NewSastConverterForTest().Convert(source)

	assert.Equal(t, actual, string(report))
}
