package patch

import "github.com/owenrumney/go-sarif/v2/sarif"

type Patch struct {
	report *sarif.Report
}

func (p Patch) Patch() {
	p.supplementLocations()
}

func NewPatch(report *sarif.Report) Patch {
	return Patch{
		report: report,
	}
}

func (p Patch) supplementLocations() {
	for _, run := range p.report.Runs {
		for _, result := range run.Results {
			for _, location := range result.Locations {
				pl := location.PhysicalLocation
				if *pl.ArtifactLocation.URI == "" {
					uri := "."
					pl.ArtifactLocation.URI = &uri
				}
			}
		}
	}
}
