package html

import (
	_ "embed"
	"github.com/stretchr/testify/assert"
	"testing"
)

//go:embed semgrep.sarif
var source []byte

//go:embed sarif-report.html
var html string

func TestConvertToHtml(t *testing.T) {
	report, _ := NewHtmlConverter().Convert(source)

	assert.Equal(t, html, string(report))
}
