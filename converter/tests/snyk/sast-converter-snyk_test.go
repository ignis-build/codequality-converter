package snyk

import (
	_ "embed"
	"github.com/stretchr/testify/assert"
	"sarif-converter/converter/sast"
	"testing"
)

//go:embed snyk_code.sarif
var code []byte

//go:embed snyk_deps.sarif
var deps []byte

//go:embed snyk_no-rule-name.sarif
var noRuleName []byte

//go:embed snyk_code_sast.json
var actualCode string

//go:embed snyk_deps_sast.json
var actualDeps string

//go:embed snyk_no-rule-name_sast.json
var actualNoRuleName string

func TestConvertFromSnykCode(t *testing.T) {
	report, _ := sast.NewSastConverterForTest().Convert(code)

	assert.Equal(t, actualCode, string(report))
}

func TestConvertFromSnykDeps(t *testing.T) {
	report, _ := sast.NewSastConverterForTest().Convert(deps)

	assert.Equal(t, actualDeps, string(report))
}

func TestConvertFromSnykNoRuleName(t *testing.T) {
	report, _ := sast.NewSastConverterForTest().Convert(noRuleName)

	assert.Equal(t, actualNoRuleName, string(report))
}
