package eslint

import (
	_ "embed"
	"github.com/stretchr/testify/assert"
	"sarif-converter/converter/codequality"
	"sarif-converter/converter/tests/codequalityreport"
	"testing"
)

//go:embed eslint.sarif
var sarif []byte

func TestConvertFromEslintSarif(t *testing.T) {
	report, _ := codequality.NewCodeQualityConverter().Convert(sarif)

	result := codequalityreport.Parse(report)

	assert.Equal(t, "eval with argument of type Identifier", *result.Elements[0].Description)
}
