package resharper

import (
	_ "embed"
	"github.com/stretchr/testify/assert"
	"sarif-converter/converter/codequality"
	"testing"
)

//go:embed resharper-no-inspections.sarif
var resharperNoInspections []byte

//go:embed resharper.sarif
var resharper []byte

//go:embed codequality.json
var codeQuality string

func TestConvertFromReSharperInspectCode(t *testing.T) {
	report, _ := codequality.NewCodeQualityConverter().Convert(resharper)

	assert.Equal(t, codeQuality, string(report))
}

func TestConvertFromReSharperInspectCodeNoInspections(t *testing.T) {
	report, _ := codequality.NewCodeQualityConverter().Convert(resharperNoInspections)

	assert.Equal(t, "[]", string(report))
}
