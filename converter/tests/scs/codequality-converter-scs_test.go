package scs

import (
	_ "embed"
	"encoding/json"
	"github.com/owenrumney/go-sarif/v2/sarif"
	"github.com/stretchr/testify/assert"
	"sarif-converter/converter/codequality"
	"sarif-converter/converter/tests/codequalityreport"
	"sarif-converter/filter"
	"testing"
)

//go:embed security-scan.sarif
var source []byte

func TestConvertFromSecurityCodeScan(t *testing.T) {
	bytes := convertToRelativePath(source, "file:///home/masakura/tmp/sc")
	report, _ := codequality.NewCodeQualityConverter().Convert(bytes)

	result := codequalityreport.Parse(report)

	assert.Equal(t, "Controllers/HomeController.cs", *result.Elements[0].Location.Path)
}

func convertToRelativePath(input []byte, srcRoot string) []byte {
	s := toSarif(input)

	rpf := filter.NewRelativePathFilter(srcRoot)
	rpf.Run(s)
	bytes := sarifToBytes(s)
	return bytes
}

func sarifToBytes(report *sarif.Report) []byte {
	j, err := json.Marshal(report)
	if err != nil {
		panic(err)
	}
	return j
}

func toSarif(report []byte) *sarif.Report {
	s, err := sarif.FromBytes(report)
	if err != nil {
		panic(err)
	}
	return s
}
