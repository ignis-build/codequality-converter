package binskim

import (
	_ "embed"
	"github.com/stretchr/testify/assert"
	"sarif-converter/converter/codequality"
	"sarif-converter/converter/tests/codequalityreport"
	"testing"
)

//go:embed binskim.sarif
var sarif []byte

func TestConvertFromBinSkimSarif(t *testing.T) {
	report, _ := codequality.NewCodeQualityConverter().Convert(sarif)

	result := codequalityreport.Parse(report)

	assert.Nil(t, result.Elements[0].Location.Lines)
	assert.Equal(t, "Application code should be compiled with the Spectre mitigations switch (/Qspectre) and toolsets that support it.", *result.Elements[0].Description)
}
