package kind

import (
	_ "embed"
	"github.com/stretchr/testify/assert"
	"sarif-converter/converter/codequality"
	"testing"
)

//go:embed kind.sarif
var sarif []byte

//go:embed kind-codequality.json
var actual string

func TestConvert_FilterByKind(t *testing.T) {
	report, _ := codequality.NewCodeQualityConverter().Convert(sarif)

	assert.Equal(t, actual, string(report))
}
