package codequalityreport

import (
	"encoding/json"
	"sarif-converter/codequality/element"
)

type CodeQuality struct {
	Elements []element.Element
}

func Parse(report []byte) CodeQuality {
	var result []element.Element
	err := json.Unmarshal(report, &result)
	if err != nil {
		panic(err)
	}
	return CodeQuality{
		Elements: result,
	}
}
