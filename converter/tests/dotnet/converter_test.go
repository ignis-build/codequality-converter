package dotnet

import (
	_ "embed"
	"github.com/stretchr/testify/assert"
	"sarif-converter/converter/codequality"
	"testing"
)

//go:embed compiler-diagnostics.sarif
var sarif []byte

//go:embed gl-codequality-report.json
var codequalityReport string

func TestConvert_CodeQuality_DotNet(t *testing.T) {
	report, _ := codequality.NewCodeQualityConverter().Convert(sarif)

	assert.Equal(t, codequalityReport, string(report))
}
