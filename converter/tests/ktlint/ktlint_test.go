package ktlint

import (
	_ "embed"
	"github.com/stretchr/testify/assert"
	"sarif-converter/converter/codequality"
	"sarif-converter/filter"
	"sarif-converter/main/argument"
	"testing"
)

//go:embed ktlint.sarif
var source []byte

//go:embed codequality.json
var actual string

func TestConvertKtlint(t *testing.T) {
	b := filterSourceRoot("/builds/jetbrains-ide-plugins/semgrep-plugin")

	report, _ := codequality.NewCodeQualityConverter().Convert(b)

	assert.Equal(t, actual, string(report))
}

func filterSourceRoot(srcRoot string) []byte {
	arg, _ := argument.Parse([]string{
		"sarif-converter",
		"--src-root=" + srcRoot,
	})
	b, _ := filter.AllSarifFilter(source, arg)

	return b
}
