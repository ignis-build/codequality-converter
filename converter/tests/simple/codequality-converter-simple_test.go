package simple

import (
	_ "embed"
	"github.com/stretchr/testify/assert"
	"sarif-converter/converter/codequality"
	"testing"
)

//go:embed semgrep.sarif
var sarif []byte

//go:embed actual.json
var actual string

func TestConvert(t *testing.T) {
	report, _ := codequality.NewCodeQualityConverter().Convert(sarif)

	assert.Equal(t, actual, string(report))
}
