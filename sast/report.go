package sast

import (
	"bytes"
	"encoding/json"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
	"sarif-converter/meta"
	"sarif-converter/now"
	wrapper "sarif-converter/sarifreport/report"
	"sarif-converter/sast/sarif"
)

type Report struct {
	sast *report.Report
}

func (r Report) Json() ([]byte, error) {
	return json.MarshalIndent(r.sast, "", "  ")
}

func ConvertFrom(report *wrapper.Wrapper, time now.TimeProvider, metadata meta.Metadata) (*Report, error) {
	filtered := report.OnlyRequireReport()

	sast, err := transformToGLSASTReport(filtered)
	if err != nil {
		return nil, err
	}

	sast = sast.supplementVulnerabilitiesIdentifiersName()
	sast = sast.supplementScan(filtered, time, metadata)
	return &sast, nil
}

func (r Report) supplementVulnerabilitiesIdentifiersName() Report {
	s := *r.sast
	for i1 := range s.Vulnerabilities {
		v := &s.Vulnerabilities[i1]
		for i2 := range v.Identifiers {
			i := &v.Identifiers[i2]
			if i.Name == "" {
				i.Name = i.Value
			}
		}
	}
	return Report{sast: &s}
}

func (r Report) supplementScan(report *wrapper.Wrapper, time now.TimeProvider, metadata meta.Metadata) Report {
	overrider := sarif.NewReport(report.Value())
	if time != nil {
		overrider = overrider.WithTimeProvider(time)
	}

	sast := *r.sast
	overrides := overrider.OverrideScan(sast.Scan, metadata)
	overrides.Type = "sast"
	sast.Scan = overrides

	return Report{sast: &sast}
}

func (r Report) Fix() Report {
	if r.sast.Vulnerabilities == nil {
		r.sast.Vulnerabilities = []report.Vulnerability{}
	}
	return r
}

func transformToGLSASTReport(input *wrapper.Wrapper) (Report, error) {
	b, err := input.Bytes()
	if err != nil {
		return Report{}, err
	}

	gf := newGitLabFeatures()

	gf.unset()
	sast, err := report.TransformToGLSASTReport(bytes.NewReader(b), "", "", report.Scanner{})
	gf.restore()

	return Report{sast: sast}, err
}
