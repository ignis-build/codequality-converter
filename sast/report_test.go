package sast

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
	"testing"
)

func TestSupplementVulnerabilitiesIdentifiersName(t *testing.T) {
	r := Report{
		sast: &report.Report{
			Vulnerabilities: []report.Vulnerability{
				{Identifiers: newIdentifiers("Name1", "Value1")},
				{Identifiers: newIdentifiers("", "Value2")},
			},
		},
	}.supplementVulnerabilitiesIdentifiersName()

	actual, _ := r.Json()

	expected, _ := Report{
		sast: &report.Report{
			Vulnerabilities: []report.Vulnerability{
				{Identifiers: newIdentifiers("Name1", "Value1")},
				{Identifiers: newIdentifiers("Value2", "Value2")},
			},
		},
	}.Json()

	assert.Equal(t, string(expected), string(actual))
}

func newIdentifiers(name string, value string) []report.Identifier {
	return []report.Identifier{
		newIdentifier(name, value),
	}
}

func newIdentifier(name string, value string) report.Identifier {
	return report.Identifier{Name: name, Value: value}
}

func (r Report) identifiers() []report.Identifier {
	var identifiers []report.Identifier
	for _, v := range r.sast.Vulnerabilities {
		for _, i := range v.Identifiers {
			identifiers = append(identifiers, i)
		}
	}
	return identifiers
}
