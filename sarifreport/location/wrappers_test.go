package location

import (
	"github.com/owenrumney/go-sarif/v2/sarif"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestWrappers_Path(t *testing.T) {
	target := NewWrappers([]*sarif.Location{
		{
			PhysicalLocation: &sarif.PhysicalLocation{
				ArtifactLocation: &sarif.ArtifactLocation{
					URI: p("file.txt"),
				},
			},
		},
	})

	assert.Equal(t, "file.txt", target.Path())
}

func TestWrappers_Path_NilURI(t *testing.T) {
	target := NewWrappers([]*sarif.Location{
		{
			PhysicalLocation: &sarif.PhysicalLocation{
				ArtifactLocation: &sarif.ArtifactLocation{},
			},
		},
	})

	assert.Equal(t, "null", target.Path())
}

func TestWrappers_Path_NilArtifactLocation(t *testing.T) {
	target := NewWrappers([]*sarif.Location{
		{
			PhysicalLocation: &sarif.PhysicalLocation{},
		},
	})

	assert.Equal(t, "null", target.Path())
}

func TestWrappers_Path_NilPhysicalLocation(t *testing.T) {
	target := NewWrappers([]*sarif.Location{{}})

	assert.Equal(t, "null", target.Path())
}

func TestWrappers_Path_EmptyLocations(t *testing.T) {
	target := NewWrappers([]*sarif.Location{})

	assert.Equal(t, "null", target.Path())
}

func TestWrapper_StartLine(t *testing.T) {
	target := NewWrappers([]*sarif.Location{
		{
			PhysicalLocation: &sarif.PhysicalLocation{
				Region: &sarif.Region{
					StartLine: pi(5),
				},
			},
		},
	})

	assert.Equal(t, 5, *target.StartLine())
}

func TestWrapper_StartLine_NilStartLine(t *testing.T) {
	target := NewWrappers([]*sarif.Location{
		{
			PhysicalLocation: &sarif.PhysicalLocation{
				Region: &sarif.Region{},
			},
		},
	})

	assert.Nil(t, target.StartLine())
}

func TestWrapper_StartLine_NilPhysicalLocation(t *testing.T) {
	target := NewWrappers([]*sarif.Location{
		{
			PhysicalLocation: &sarif.PhysicalLocation{},
		},
	})

	assert.Nil(t, target.StartLine())
}

func TestWrapper_StartLine_NilLocation(t *testing.T) {
	target := NewWrappers([]*sarif.Location{{}})

	assert.Nil(t, target.StartLine())
}

func pi(i int) *int {
	return &i
}

func p(s string) *string {
	return &s
}
