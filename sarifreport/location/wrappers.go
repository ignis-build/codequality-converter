package location

import "github.com/owenrumney/go-sarif/v2/sarif"

type Wrappers struct {
	locations []Wrapper
}

func (w Wrappers) Path() string {
	return w.firstOrEmpty().Path()
}

func (w Wrappers) StartLine() *int {
	return w.firstOrEmpty().StartLine()
}

func (w Wrappers) firstOrEmpty() Wrapper {
	if len(w.locations) > 0 {
		return w.locations[0]
	}
	return EmptyWrapper()
}

func NewWrappers(locations []*sarif.Location) Wrappers {
	list := make([]Wrapper, len(locations))
	for i, location := range locations {
		list[i] = NewWrapper(location)
	}
	return Wrappers{
		locations: list,
	}
}
