package location

import (
	"github.com/owenrumney/go-sarif/v2/sarif"
)

type Wrapper struct {
	location *sarif.Location
}

func (w Wrapper) Path() string {
	s := w.uri()
	if s == nil {
		return "null"
	}
	return *s
}

func (w Wrapper) StartLine() *int {
	r := w.physicalLocationOrEmpty().Region
	if r == nil {
		return nil
	}

	return r.StartLine
}

func (w Wrapper) uri() *string {
	p := w.physicalLocationOrEmpty()
	if p == nil {
		return nil
	}

	a := p.ArtifactLocation
	if a == nil {
		return nil
	}

	return a.URI
}

func (w Wrapper) locationOrEmpty() *sarif.Location {
	if w.location != nil {
		return w.location
	}
	return &sarif.Location{}
}

func (w Wrapper) physicalLocationOrEmpty() *sarif.PhysicalLocation {
	p := w.locationOrEmpty().PhysicalLocation
	if p != nil {
		return p
	}

	return &sarif.PhysicalLocation{}
}

func NewWrapper(location *sarif.Location) Wrapper {
	return Wrapper{location: location}
}

func EmptyWrapper() Wrapper {
	return NewWrapper(nil)
}
